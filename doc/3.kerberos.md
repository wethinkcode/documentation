# Chapter 3 - Kerberos
### 3.1 Introduction

Kerberos /ˈkərbərɒs/ is a computer network authentication protocol that works on the basis of 'tickets' to allow nodes communicating over a non-secure network to prove their identity to one another in a secure manner.

### 3.2 Installation


Ignore the configuration questions during the installation.

```
sudo apt-get install ldap-auth-client libpam-krb5 krb5-user libpam-foreground libsasl2-modules-gssapi-mit
```
^^^^^******** NOT WORKING *******^^^^
```
apt install krb5-kdc krb5-admin-server krb5-kdc-ldap
```

vim /etc/krb5.conf
```
[libdefaults]
	default_realm = WETHINKCODE.CO.ZA
	krb4_config = /etc/krb.conf
	kdc_timesync = 1
	ccache_type = 4
	forwardable = true
	proxiable = true

[realms]
	WTC.CO.ZA = {
		kdc = kdc-wethinkcode.wethinkcode.co.za
		admin_server = kdc-wethinkcode.wethinkcode.co.za
		default_domain = wethinkcode.co.za
		database_module = openldap_ldapconf
	}

[domain_realm]
	.wethinkcode.co.za = WETHINKCODE.CO.ZA

[dbdefaults]
	ldap_kerberos_container_dn = cn=krbContainer,dc=wethinkcode,dc=co,dc=za

[dbmodules]
        openldap_ldapconf = {
                db_library = kldap
                ldap_kdc_dn = "cn=admin,dc=wethinkcode,dc=co,dc=za"

                # this object needs to have read rights on
                # the realm container, principal container and realm sub-trees
                ldap_kadmind_dn = "cn=admin,dc=wethinkcode,dc=co,dc=za"

                # this object needs to have read and write rights on
                # the realm container, principal container and realm sub-trees
                ldap_service_password_file = /etc/krb5kdc/service.keyfile
                ldap_servers = ldap://ldap-wethinkcode.wethinkcode.co.za
                ldap_conns_per_server = 5
        }

[login]
	krb4_convert = true
	krb4_get_tickets = false

[logging]
	kdc = FILE:/var/log/kdc.log
	default = FILE:/var/log/kdc.log
	admin_server = FILE:/var/log/kadmin.log
```

Create the realm
```
kdb5_ldap_util -D cn=admin,dc=wethinkcode,dc=co,dc=za create -subtrees \
dc=wethinkcode,dc=co,dc=za -r WETHINKCODE.CO.ZA -s -H ldap://ldap-wethinkcode.wethinkcode.co.za
```

Create the stash for the LDAP bind password
```
kdb5_ldap_util -D  cn=admin,dc=example,dc=com stashsrvpw -f \
/etc/krb5kdc/service.keyfile cn=admin,dc=wethinkcode,dc=co,dc=za
```

Copy a valid CA certificate into **/etc/ssl/certs** or get one from the LDAP server:
```
scp ldap01:/etc/ssl/certs/cacert.pem .
sudo cp cacert.pem /etc/ssl/certs
```
Edit **/etc/ldap/ldap.conf** to use the certificate:
```
TLS_CACERT /etc/ssl/certs/cacert.pem
The certificate will also need to be copied to the Secondary KDC, to allow the connection to the LDAP servers using LDAPS.
```
Start the Kerberos KDC and admin server:
```
service krb5-kdc start
service krb5-admin-server start
```

### 3.3 Configuring Kerberos
