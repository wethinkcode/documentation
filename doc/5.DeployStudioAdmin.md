# Chapter 5 - DeployStudioAdmin
### 5.1 Introduction

Saving time is a daily concern for system administrators, especially with the increase of minor interventions requested by end-users: accidentally modified system preferences, unstable or misconfigured third-party applications, wrong access rights, viruses, etc...

macOS is stable and robust but the capacity to (re)install completely a workstation or a server in a few minutes following a preset workflow that automatically restores the right disk image, configures and backs up what was planned by the administrator is a real benefit in terms of time and money. DeployStudio was designed this way, in order to manage flawlessly large deployments of workstations and servers.

You will require either a Mac Mini or iMac. The storage will be hosted on the nearest NAS / ZFS.

`OS : Debian 9`
<br>
`RAM : 8192MB`
<br>
`Storage : 1TB`

### 5.2 Installation

http://www.techrepublic.com/article/how-to-install-and-configure-deploystudio/

Download and open the DeployStudio DMG file. Inside you'll find the package installer. Execute it to begin the installation process
http://www.deploystudio.com/downloads
