# APT-GET INSTALL WETHINKCODE

## Version 0.1

Maxime ROUX max@wethinkcode.co.za  
Arno van WYK arno@wethinkcode.co.za  
Geoffrey LEGER geff@wethinkcode.co.za  
Camille AGON camille@wethinkcode.co.za  
Arlene MULDER arlene@wethinkcode.co.za

_Summary: How to create a REALLY cool IT infrastructure._


## Contents

- 1 Prelude
   - 1.1 Introduction
- 2 OpenLDAP
   - 2.1 Introduction
   - 2.2 Installation
- 3 Kerberos
   - 3.1 Introduction
   - 3.2 Installation
- 4 Ansible
   - 4.1 Introduction
   - 4.2 Installation
- 5 DeployStudioAdmin
   - 5.1 Introduction
   - 5.2 Installation

# Chapter 1 - Prelude

### 1.1 Introduction

```
This represent what's in your terminal, the input, output, etc.
```

Each `$` in a terminal represent your prompt

Example
```
$ apt-get update
```


# Chapter 2 - OpenLDAP
### 2.1 Introduction

OpenLDAP is a free, open source implementation of the Lightweight Directory Access Protocol (LDAP) developed by the OpenLDAP Project. It is released under its own BSD-style license called the OpenLDAP Public License.

LDAP is a platform-independent protocol. Several common Linux distributions include OpenLDAP Software for LDAP support. The software also runs on BSD-variants, as well as AIX, Android, HP-UX, macOS, Solaris, Microsoft Windows (NT and derivatives, e.g. 2000, XP, Vista, Windows 7, etc.), and z/OS.

### 2.2 Installation



Install the OpenLDAP server daemon and the traditional LDAP management utilities. These are found in packages slapd and ldap-utils respectively.

```
$ apt-get update
$ apt-get upgrade
$ apt-get dist-upgrade
$ apt install slapd ldap-utils
```


Set DNS domain name to wethinkcode.co.za

```
$ dpkg-reconfigure slapd
```


>Omit OpenLDAP server configuration? **No**

>DNS domain name? **wethinkcode.co.za**

>Organization name? **wethinkcode.co.za**

>Administrator password? enter a secure password twice

>Database backend? **MDB**

>Remove the database when slapd is purged? **No**

>Move old database? **Yes**

>Allow LDAPv2 protocol? **No**


```
$ apt-get update
$ apt-get upgrade
$ apt-get dist-upgrade
$ apt install phpldapadmin
```

Configure phpLDAPadmin
------------------------

`$ vim /etc/phpldapadmin/config.php`

Give the server a name

`$servers->setValue('server','host','LDAP Master Server');`

Change your domain
```
$servers->setValue('server','base',array('dc=wethinkcode,dc=co,dc=za'));
```
Change your distinguished name
```
$servers->setValue('login','bind_id','cn=admin,dc=wethinkcode,dc=co,dc=za');
```
Uncomment and set to true
```
$config->custom->appearance['hide_template_warning'] = true;
```

Secure Apache
-----------------------
```
$ mkdir /etc/apache3/ssl
```
Copy your certificate into /etc/apache3/ssl or generate your own with:
```
$ openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/apache2/ssl/apache.key -out /etc/apache2/ssl/apache.crt
```
Enable SSL
```
a2enmod ssl
```
```
vim apt-get install phpldapadmin
```
Set URL location to /phpldapadmin
Edit Apache configuration
```
vim /etc/phpldapadmin/apache.conf
```
<IfModule mod_alias.c>
    Alias /phpldapadmin /usr/share/phpldapadmin/htdocs
</IfModule>
```

Configure the HTTP Virtual Host
--------------------------------

```
$ vim /etc/apache2/sites-enabled/000-default.conf

<VirtualHost *:80>
    ServerAdmin webmaster@**server_domain_or_IP**
    DocumentRoot /var/www/html
    **ServerName server_domain_or_IP**
    **Redirect permanent /superldap https://server_domain_or_IP/phpldapadmin**
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Enable the SSL Virtual Host file.
a2ensite default-ssl.conf

```
$ vim /etc/apache2/sites-enabled/default-ssl.conf
...
ServerAdmin webmaster@**server_domain_or_IP**
**ServerName server_domain_or_IP**
SSLCertificateFile **/etc/apache2/ssl/apache.crt**
SSLCertificateKeyFile **/etc/apache2/ssl/apache.key**
...
$ service apache2 restart
```


### 2.3 Configuring LDAP for Kerberos

First, the necessary schema needs to be loaded
To load the schema into LDAP, on the LDAP server install the krb5-kdc-ldap package.
```
$ apt-get update
$ apt-get upgrade
$ apt-get dist-upgrade
$ apt install krb5-kdc-ldap
```
Next, extract the kerberos.schema.gz file
```
$ gzip -d /usr/share/doc/krb5-kdc-ldap/kerberos.schema.gz
$ cp /usr/share/doc/krb5-kdc-ldap/kerberos.schema /etc/ldap/schema/
```
The kerberos schema needs to be added to the cn=config tree.

Create a configuration file named schema_convert.conf, or a similar descriptive name, containing the following lines
```
include /etc/ldap/schema/core.schema
include /etc/ldap/schema/collective.schema
include /etc/ldap/schema/corba.schema
include /etc/ldap/schema/cosine.schema
include /etc/ldap/schema/duaconf.schema
include /etc/ldap/schema/dyngroup.schema
include /etc/ldap/schema/inetorgperson.schema
include /etc/ldap/schema/java.schema
include /etc/ldap/schema/misc.schema
include /etc/ldap/schema/nis.schema
include /etc/ldap/schema/openldap.schema
include /etc/ldap/schema/ppolicy.schema
include /etc/ldap/schema/kerberos.schema
```
Create a temporary directory to hold the LDIF files
```
$ mkdir /tmp/ldif_output
```
Now use slapcat to convert the schema files:
```
$ slapcat -f schema_convert.conf -F /tmp/ldif_output -n0 -s \
"cn={12}kerberos,cn=schema,cn=config" > /tmp/cn=kerberos.ldif
```

Edit the generated /tmp/cn\=kerberos.ldif file, changing the following attributes:
```
dn: cn=kerberos,cn=schema,cn=config
...
cn: kerberos
```

And remove the following lines from the end of the file:
```
structuralObjectClass: olcSchemaConfig
entryUUID: 18ccd010-746b-102d-9fbe-3760cca765dc
creatorsName: cn=config
createTimestamp: 20090111203515Z
entryCSN: 20090111203515.326445Z#000000#000#000000
modifiersName: cn=config
modifyTimestamp: 20090111203515Z
```

The attribute values will vary, just be sure the attributes are removed.
Load the new schema with ldapadd:
```
$ ldapadd -Q -Y EXTERNAL -H ldapi:/// -f /tmp/cn\=kerberos.ldif
```

Add an index for the krb5principalname attribute.
Create a new ldif to update the Access Control Lists (ACL)
```
$ vim /etc/ldap/slapd.d/cn\=config/olcDatabase\=\{1\}mdb.ldif
```
Add the following
```
olcDbIndex: krbPrincipalName eq,pres,sub
```
Remove the **olcAccess** lines and replace them with the following
```
olcAccess: {0}to attrs=userPassword,shadowLastChange,krbPrincipalKey by dn="cn=admin,dc=example,dc=com" write by anonymous auth by self write by * none
olcAccess: {1}to dn.base="" by * read
olcAccess: {2}to * by dn="cn=admin,dc=example,dc=com" write by * read
```
Recaluculate the CRC32 checksum
```
$ apt install libarchive-zip-perl
$ crc32 <(cat /etc/ldap/slapd.d/cn\=config/olcDatabase\=\{1\}mdb.ldif | tail -n +3)
```
We add a | tail -n +3 because the checksum has to be calculated on the actual
content of the file, without the first two lines.
You will get the CRC32 value that you must put at the begining of the file:
```
# AUTO-GENERATED FILE - DO NOT EDIT!! Use ldapmodify.
# CRC32 297b26c4
```


!!!!!!!!!!!!!!!!!!!!!!!!!!

Load schemas in this order
1.42
2.samba
3.apple-aux
4.apple

!!!!!!!!!!!!!!!!!!!!!!!!!!

ON KDC:
---------------------------------
Create ldap-wethikcode principal
```
kadmin.local
ktadd -k /krb5.keytab ldap/ldap-wethinkcode.wethinkcode.co.za
```

Create kerberos keytab
```
ktadd -k /krb5.keytab ldap/ldap-wethinkcode.wethinkcode.co.za@WETHINKCODE.CO.ZA
```
scp keytab to ldap

copy keytab to /etc/krb5.keytab
if selecting a custom location, edit /etc/default/slapd
uncomment
```
export KRB5_KTNAME=/etc/krb5.keytab
```

edit /etc/krb5.conf
```
[libdefaults]
        default_realm = WETHINKCODE.CO.ZA

# The following krb5.conf variables are only for MIT Kerberos.
        kdc_timesync = 1
        ccache_type = 4
        forwardable = true
        proxiable = true
        dns_lookup_kdc = no
        dns_lookup_realm = no
        allow_weak_crypto = true

# The following encryption type specification will be used by MIT Kerberos
# if uncommented.  In general, the defaults in the MIT Kerberos code are
# correct and overriding these specifications only serves to disable new
# encryption types as they are added, creating interoperability problems.
#
# The only time when you might need to uncomment these lines and change
# the enctypes is if you have local software that will break on ticket
# caches containing ticket encryption types it doesn't know about (such as
# old versions of Sun Java).

#       default_tgs_enctypes = des3-hmac-sha1
#       default_tkt_enctypes = des3-hmac-sha1
#       permitted_enctypes = des3-hmac-sha1

# The following libdefaults parameters are only for Heimdal Kerberos.
#       fcc-mit-ticketflags = true

[realms]
        WETHINKCODE.CO.ZA = {
#               kdc = 10.242.0.122
                kdc = kdc-wethinkcode.wethinkcode.co.za
#               admin_server = 10.242.0.122
                admin_server = kdc-wethinkcode.wethinkcode.co.za
                default_domain = wethinkcode.co.za
                database_module = openldap_ldapconf
        }

[login]
    krb4_convert = true
    krb4_get_tickets = false


[domain_realm]
    .wethinkcode.co.za = WETHINKCODE.CO.ZA
    wethinkcode.co.za = WETHINKCODE.CO.ZA
'''

ON LDAP
---------------------------

kinit ldap/ldap-wethinkcode.wethinkcode.co.za@WETHINKCODE.CO.ZA -k -t /etc/krb5.keytab
`ldapsearch` to test







# Chapter 3 - Kerberos
### 3.1 Introduction

Kerberos /ˈkərbərɒs/ is a computer network authentication protocol that works on the basis of 'tickets' to allow nodes communicating over a non-secure network to prove their identity to one another in a secure manner.

### 3.2 Installation


Ignore the configuration questions during the installation.

```
sudo apt-get install ldap-auth-client libpam-krb5 krb5-user libpam-foreground libsasl2-modules-gssapi-mit
```
^^^^^******** NOT WORKING *******^^^^
```
apt install krb5-kdc krb5-admin-server krb5-kdc-ldap
```

vim /etc/krb5.conf
```
[libdefaults]
	default_realm = WETHINKCODE.CO.ZA
	krb4_config = /etc/krb.conf
	kdc_timesync = 1
	ccache_type = 4
	forwardable = true
	proxiable = true

[realms]
	WTC.CO.ZA = {
		kdc = kdc-wethinkcode.wethinkcode.co.za
		admin_server = kdc-wethinkcode.wethinkcode.co.za
		default_domain = wethinkcode.co.za
		database_module = openldap_ldapconf
	}

[domain_realm]
	.wethinkcode.co.za = WETHINKCODE.CO.ZA

[dbdefaults]
	ldap_kerberos_container_dn = cn=krbContainer,dc=wethinkcode,dc=co,dc=za

[dbmodules]
        openldap_ldapconf = {
                db_library = kldap
                ldap_kdc_dn = "cn=admin,dc=wethinkcode,dc=co,dc=za"

                # this object needs to have read rights on
                # the realm container, principal container and realm sub-trees
                ldap_kadmind_dn = "cn=admin,dc=wethinkcode,dc=co,dc=za"

                # this object needs to have read and write rights on
                # the realm container, principal container and realm sub-trees
                ldap_service_password_file = /etc/krb5kdc/service.keyfile
                ldap_servers = ldap://ldap-wethinkcode.wethinkcode.co.za
                ldap_conns_per_server = 5
        }

[login]
	krb4_convert = true
	krb4_get_tickets = false

[logging]
	kdc = FILE:/var/log/kdc.log
	default = FILE:/var/log/kdc.log
	admin_server = FILE:/var/log/kadmin.log
```

Create the realm
```
kdb5_ldap_util -D cn=admin,dc=wethinkcode,dc=co,dc=za create -subtrees \
dc=wethinkcode,dc=co,dc=za -r WETHINKCODE.CO.ZA -s -H ldap://ldap-wethinkcode.wethinkcode.co.za
```

Create the stash for the LDAP bind password
```
kdb5_ldap_util -D  cn=admin,dc=example,dc=com stashsrvpw -f \
/etc/krb5kdc/service.keyfile cn=admin,dc=wethinkcode,dc=co,dc=za
```

Copy a valid CA certificate into **/etc/ssl/certs** or get one from the LDAP server:
```
scp ldap01:/etc/ssl/certs/cacert.pem .
sudo cp cacert.pem /etc/ssl/certs
```
Edit **/etc/ldap/ldap.conf** to use the certificate:
```
TLS_CACERT /etc/ssl/certs/cacert.pem
The certificate will also need to be copied to the Secondary KDC, to allow the connection to the LDAP servers using LDAPS.
```
Start the Kerberos KDC and admin server:
```
service krb5-kdc start
service krb5-admin-server start
```

### 3.3 Configuring Kerberos


# Chapter 4 - Ansible
### 4.1 Introduction

Ansible is an open-source automation engine that automates software provisioning, configuration management, and application deployment.

You will require the following specs for that Virtual Machine.

`OS : Debian 9`

`RAM : 4096MB`

`Storage : 100Gb`


### 4.2 Installation

Update your packages and install ansible

```
$ apt-get update
$ apt-get upgrade
$ apt-get dist-upgrade
$ apt-get install vim
$ apt-get install git
$ apt-get install resolvconf
$ apt-get install ansible
```

Check the version of ansible

```
$ ansible --version
ansible 2.2.1.0
  config file = /etc/ansible/ansible.cfg
  configured module search path = ['/usr/share/my_modules/']
```

Ansible primarily communicates with client computers through SSH. That's why you need to generated a key for that, or use another one that you already have. In which case, just skip the following.

We will consider that the private and public key are respectively called `id_rsa` and `id_rsa.pub`, saved in `/root/.ssh/`

```
$ ssh-keygen
(generate the key, don't put a passphrase on it !!!)
$ chmod 600 /root/.ssh/id_rsa
$ chmod 644 /root/.ssh/id_rsa.pub
```

Change the settings in `/etc/ansible/ansible.cfg

```
# config file for ansible -- http://ansible.com/
# ==============================================

# nearly all parameters can be overridden in ansible-playbook
# or with command line flags. ansible will read ANSIBLE_CONFIG,
# ansible.cfg in the current working directory, .ansible.cfg in
# the home directory or /etc/ansible/ansible.cfg, whichever it
# finds first

[defaults]

# some basic default values...

inventory      = /etc/ansible/hosts
library        = /usr/share/my_modules/
remote_tmp     = ~/.ansible/tmp
local_tmp      = ~/.ansible/tmp
forks          = 5
poll_interval  = 15
sudo_user      = root
ask_sudo_pass  = False
ask_pass       = False
transport      = smart
remote_port    = 4222
module_lang    = C
module_set_locale = False

# plays will gather facts by default, which contain information about
# the remote system.
#
# smart - gather by default, but don't regather if already gathered
# implicit - gather by default, turn off with gather_facts: False
# explicit - do not gather by default, must say gather_facts: True
gathering = smart

# by default retrieve all facts subsets
# all - gather all subsets
# network - gather min and network facts
# hardware - gather hardware facts (longest facts to retrieve)
# virtual - gather min and virtual facts
# facter - import facts from facter
# ohai - import facts from ohai
# You can combine them using comma (ex: network,virtual)
# You can negate them using ! (ex: !hardware,!facter,!ohai)
# A minimal set of facts is always gathered.
#gather_subset = all

# some hardware related facts are collected
# with a maximum timeout of 10 seconds. This
# option lets you increase or decrease that
# timeout to something more suitable for the
# environment.
# gather_timeout = 10

# additional paths to search for roles in, colon separated
#roles_path    = /etc/ansible/roles

# uncomment this to disable SSH key host checking
host_key_checking = False

# change the default callback
#stdout_callback = skippy
# enable additional callbacks
#callback_whitelist = timer, mail

# Determine whether includes in tasks and handlers are "static" by
# default. As of 2.0, includes are dynamic by default. Setting these
# values to True will make includes behave more like they did in the
# 1.x versions.
#task_includes_static = True
#handler_includes_static = True

# Controls if a missing handler for a notification event is an error or a warning
#error_on_missing_handler = True

# change this for alternative sudo implementations
sudo_exe = sudo

# What flags to pass to sudo
# WARNING: leaving out the defaults might create unexpected behaviours
#sudo_flags = -H -S -n

# SSH timeout
timeout = 20

# default user to use for playbooks if user is not specified
# (/usr/bin/ansible will use current user as default)
remote_user = root

# logging is off by default unless this path is defined
# if so defined, consider logrotate
#log_path = /var/log/ansible.log

# default module name for /usr/bin/ansible
module_name = shell

# use this shell for commands executed under sudo
# you may need to change this to bin/bash in rare instances
# if sudo is constrained
executable = /bin/sh

# if inventory variables overlap, does the higher precedence one win
# or are hash values merged together?  The default is 'replace' but
# this can also be set to 'merge'.
#hash_behaviour = replace

# by default, variables from roles will be visible in the global variable
# scope. To prevent this, the following option can be enabled, and only
# tasks and handlers within the role will see the variables there
#private_role_vars = yes

# list any Jinja2 extensions to enable here:
#jinja2_extensions = jinja2.ext.do,jinja2.ext.i18n

# if set, always use this private key file for authentication, same as
# if passing --private-key to ansible or ansible-playbook
private_key_file = /root/.ssh/id_rsa

# If set, configures the path to the Vault password file as an alternative to
# specifying --vault-password-file on the command line.
#vault_password_file = /path/to/vault_password_file

# format of string {{ ansible_managed }} available within Jinja2
# templates indicates to users editing templates files will be replaced.
# replacing {file}, {host} and {uid} and strftime codes with proper values.
#ansible_managed = Ansible managed: {file} modified on %Y-%m-%d %H:%M:%S by {uid} on {host}
# {file}, {host}, {uid}, and the timestamp can all interfere with idempotence
# in some situations so the default is a static string:
ansible_managed = Ansible managed: {file} modified on %Y-%m-%d %H:%M:%S by {uid] on {host}

# by default, ansible-playbook will display "Skipping [host]" if it determines a task
# should not be run on a host.  Set this to "False" if you don't want to see these "Skipping"
# messages. NOTE: the task header will still be shown regardless of whether or not the
# task is skipped.
#display_skipped_hosts = True

# by default, if a task in a playbook does not include a name: field then
# ansible-playbook will construct a header that includes the task's action but
# not the task's args.  This is a security feature because ansible cannot know
# if the *module* considers an argument to be no_log at the time that the
# header is printed.  If your environment doesn't have a problem securing
# stdout from ansible-playbook (or you have manually specified no_log in your
# playbook on all of the tasks where you have secret information) then you can
# safely set this to True to get more informative messages.
#display_args_to_stdout = False

# by default (as of 1.3), Ansible will raise errors when attempting to dereference
# Jinja2 variables that are not set in templates or action lines. Uncomment this line
# to revert the behavior to pre-1.3.
#error_on_undefined_vars = False

# by default (as of 1.6), Ansible may display warnings based on the configuration of the
# system running ansible itself. This may include warnings about 3rd party packages or
# other conditions that should be resolved if possible.
# to disable these warnings, set the following value to False:
#system_warnings = True

# by default (as of 1.4), Ansible may display deprecation warnings for language
# features that should no longer be used and will be removed in future versions.
# to disable these warnings, set the following value to False:
deprecation_warnings = False

# (as of 1.8), Ansible can optionally warn when usage of the shell and
# command module appear to be simplified by using a default Ansible module
# instead.  These warnings can be silenced by adjusting the following
# setting or adding warn=yes or warn=no to the end of the command line
# parameter string.  This will for example suggest using the git module
# instead of shelling out to the git command.
# command_warnings = False


# set plugin path directories here, separate with colons
action_plugins     = /usr/share/ansible/plugins/action
cache_plugins      = /usr/share/ansible/plugins/cache
callback_plugins   = /usr/share/ansible/plugins/callback
connection_plugins = /usr/share/ansible/plugins/connection
lookup_plugins     = /usr/share/ansible/plugins/lookup
inventory_plugins  = /usr/share/ansible/plugins/inventory
vars_plugins       = /usr/share/ansible/plugins/vars
filter_plugins     = /usr/share/ansible/plugins/filter
test_plugins       = /usr/share/ansible/plugins/test
strategy_plugins   = /usr/share/ansible/plugins/strategy

# by default callbacks are not loaded for /bin/ansible, enable this if you
# want, for example, a notification or logging callback to also apply to
# /bin/ansible runs
#bin_ansible_callbacks = False


# don't like cows?  that's unfortunate.
# set to 1 if you don't want cowsay support or export ANSIBLE_NOCOWS=1
#nocows = 1

# set which cowsay stencil you'd like to use by default. When set to 'random',
# a random stencil will be selected for each task. The selection will be filtered
# against the `cow_whitelist` option below.
#cow_selection = default
#cow_selection = random

# when using the 'random' option for cowsay, stencils will be restricted to this list.
# it should be formatted as a comma-separated list with no spaces between names.
# NOTE: line continuations here are for formatting purposes only, as the INI parser
#       in python does not support them.
#cow_whitelist=bud-frogs,bunny,cheese,daemon,default,dragon,elephant-in-snake,elephant,eyes,\
#              hellokitty,kitty,luke-koala,meow,milk,moofasa,moose,ren,sheep,small,stegosaurus,\
#              stimpy,supermilker,three-eyes,turkey,turtle,tux,udder,vader-koala,vader,www

# don't like colors either?
# set to 1 if you don't want colors, or export ANSIBLE_NOCOLOR=1
#nocolor = 1

# if set to a persistent type (not 'memory', for example 'redis') fact values
# from previous runs in Ansible will be stored.  This may be useful when
# wanting to use, for example, IP information from one group of servers
# without having to talk to them in the same playbook run to get their
# current IP information.
#fact_caching = memory


# retry files
# When a playbook fails by default a .retry file will be created in ~/
# You can disable this feature by setting retry_files_enabled to False
# and you can change the location of the files by setting retry_files_save_path

#retry_files_enabled = False
#retry_files_save_path = ~/.ansible-retry

# squash actions
# Ansible can optimise actions that call modules with list parameters
# when looping. Instead of calling the module once per with_ item, the
# module is called once with all items at once. Currently this only works
# under limited circumstances, and only with parameters named 'name'.
#squash_actions = apk,apt,dnf,homebrew,package,pacman,pkgng,yum,zypper

# prevents logging of task data, off by default
#no_log = False

# prevents logging of tasks, but only on the targets, data is still logged on the master/controller
#no_target_syslog = False

# controls whether Ansible will raise an error or warning if a task has no
# choice but to create world readable temporary files to execute a module on
# the remote machine.  This option is False by default for security.  Users may
# turn this on to have behaviour more like Ansible prior to 2.1.x.  See
# https://docs.ansible.com/ansible/become.html#becoming-an-unprivileged-user
# for more secure ways to fix this than enabling this option.
#allow_world_readable_tmpfiles = False

# controls the compression level of variables sent to
# worker processes. At the default of 0, no compression
# is used. This value must be an integer from 0 to 9.
#var_compression_level = 9

# controls what compression method is used for new-style ansible modules when
# they are sent to the remote system.  The compression types depend on having
# support compiled into both the controller's python and the client's python.
# The names should match with the python Zipfile compression types:
# * ZIP_STORED (no compression. available everywhere)
# * ZIP_DEFLATED (uses zlib, the default)
# These values may be set per host via the ansible_module_compression inventory
# variable
#module_compression = 'ZIP_DEFLATED'

# This controls the cutoff point (in bytes) on --diff for files
# set to 0 for unlimited (RAM may suffer!).
#max_diff_size = 1048576

[privilege_escalation]
#become=True
#become_method=sudo
#become_user=root
#become_ask_pass=False

[paramiko_connection]

# uncomment this line to cause the paramiko connection plugin to not record new host
# keys encountered.  Increases performance on new host additions.  Setting works independently of the
# host key checking setting above.
record_host_keys=False

# by default, Ansible requests a pseudo-terminal for commands executed under sudo. Uncomment this
# line to disable this behaviour.
#pty=False

[ssh_connection]

# ssh arguments to use
# Leaving off ControlPersist will result in poor performance, so use
# paramiko on older platforms rather than removing it, -C controls compression use
ssh_args = -o ControlMaster=auto -o ControlPersist=30m -o PreferredAuthentications=publickey -o Protocol=2

# The path to use for the ControlPath sockets. This defaults to
# "%(directory)s/ansible-ssh-%%h-%%p-%%r", however on some systems with
# very long hostnames or very long path names (caused by long user names or
# deeply nested home directories) this can exceed the character limit on
# file socket names (108 characters for most platforms). In that case, you
# may wish to shorten the string below.
#
# Example:
# control_path = %(directory)s/%%h-%%r
control_path = /tmp/ansible-ssh-%%h-%%p-%%r

# Enabling pipelining reduces the number of SSH operations required to
# execute a module on the remote server. This can result in a significant
# performance improvement when enabled, however when using "sudo:" you must
# first disable 'requiretty' in /etc/sudoers
#
# By default, this option is disabled to preserve compatibility with
# sudoers configurations that have requiretty (the default on many distros).
#
pipelining = True

# Control the mechanism for transfering files
#   * smart = try sftp and then try scp [default]
#   * True = use scp only
#   * False = use sftp only
scp_if_ssh = True

# if False, sftp will not use batch mode to transfer files. This may cause some
# types of file transfer failures impossible to catch however, and should
# only be disabled if your sftp version has problems with batch mode
#sftp_batch_mode = False

[accelerate]
accelerate_port = 5099
accelerate_timeout = 30
accelerate_connect_timeout = 5.0

# The daemon timeout is measured in minutes. This time is measured
# from the last activity to the accelerate daemon.
accelerate_daemon_timeout = 30

# If set to yes, accelerate_multi_key will allow multiple
# private keys to be uploaded to it, though each user must
# have access to the system via SSH to add a new key. The default
# is "no".
accelerate_multi_key = yes

[selinux]
# file systems that require special treatment when dealing with security context
# the default behaviour that copies the existing context or uses the user default
# needs to be changed to use the file system dependent context.
#special_context_filesystems=nfs,vboxsf,fuse,ramfs

# Set this to yes to allow libvirt_lxc connections to work without SELinux.
#libvirt_lxc_noseclabel = yes

[colors]
highlight = white
verbose = blue
warn = bright purple
error = red
debug = dark gray
deprecate = purple
skip = cyan
unreachable = red
ok = green
changed = yellow
diff_add = green
diff_remove = red
diff_lines = cyan
```

Create all the different folders required for variables, roles and custom functions

```
$ mkdir /etc/ansible/group_vars
$ mkdir /etc/ansible/roles
$ mkdir /etc/ansible/library
```

Ansible keeps track of all of the servers that it knows about through a "hosts" file. We need to set up this file first before we can begin to communicate with our other computers.

```
$ vim /etc/ansible/hosts
[groups of machines]
example.wethinkcode.co.za
```

Add your ssh key into the different machines you're looking to access. You might to enter a password for the first time. ssh keys can also be added manually by an administrator.

```
$ ssh-copy-id example.wethinkcode.co.za
```

Test the connexion with all your hosts

```
$ ansible -m ping all
example.wethinkcode.co.za | SUCCESS => {
        "changed": false,
        "ping": "pong"
}
```

Create your first playbook
```
$ touch /etc/ansible/site.yml
$ vim /etc/ansible/site.yml
---
    - hosts: munki
    gather_facts: no
    roles:
    - common
    - base
```

Add the base role folders

```
mkdir /etc/ansible/roles/base/files
mkdir /etc/ansible/roles/base/handlers
mkdir /etc/ansible/roles/base/tasks
mkdir /etc/ansible/roles/base/templates
```

Add the sshd_config needed to be deploed

```
$ mkdir /etc/ansible/roles/base/files/ssh
$ vim /etc/ansible/roles/base/files/ssh/sshd_config
# What ports, IPs and protocols we listen for
Port 4222
# Use these options to restrict which interfaces/protocols sshd will bind to
#ListenAddress ::
#ListenAddress 0.0.0.0
Protocol 2
# HostKeys for protocol version 2
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_dsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
#Privilege Separation is turned on for security
UsePrivilegeSeparation yes

# Lifetime and size of ephemeral version 1 server key
KeyRegenerationInterval 3600
ServerKeyBits 1024

# Logging
SyslogFacility AUTH
LogLevel INFO

# Authentication:
LoginGraceTime 120
PermitRootLogin yes
StrictModes yes

RSAAuthentication yes
PubkeyAuthentication yes
#AuthorizedKeysFile %h/.ssh/authorized_keys

# Don't read the user's ~/.rhosts and ~/.shosts files
IgnoreRhosts yes
# For this to work you will also need host keys in /etc/ssh_known_hosts
RhostsRSAAuthentication no
# similar for protocol version 2
HostbasedAuthentication no
# Uncomment if you don't trust ~/.ssh/known_hosts for RhostsRSAAuthentication
#IgnoreUserKnownHosts yes

# To enable empty passwords, change to yes (NOT RECOMMENDED)
PermitEmptyPasswords no

# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
ChallengeResponseAuthentication no

# Change to no to disable tunnelled clear text passwords
PasswordAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosGetAFSToken no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes

X11Forwarding yes
X11DisplayOffset 10
PrintMotd no
PrintLastLog yes
TCPKeepAlive yes
#UseLogin no

#MaxStartups 10:30:60
#Banner /etc/issue.net

# Allow client to pass locale environment variables
AcceptEnv LANG LC_*

Subsystem sftp /usr/lib/openssh/sftp-server

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
UsePAM yes
```

Create your first task to copy a file, and restart a service

```
$ vim /etc/ansible/roles/base/tasks/main.yml
---
#       #
## SSH ##
#       #
-   name: Copy sshd_config
    copy: src=etc/ssh/sshd_config dest=/etc/ssh/sshd_config owner=root group=root mode=0700
    tags: ssh

-   name: Restart ssh service
    service:
        name: ssh
        state: restarted
    tags: ssh

```

Run ansible-playbook to apply the changes to the machine
```
$ ansible-playbook /etc/ansible/site.yml
PLAY [example] *******************************************************************

TASK [base : Copy sshd_config] *************************************************
ok: [example.wethinkcode.co.za]

TASK [base : Restart ssh service] **********************************************
changed: [example.wethinkcode.co.za]

PLAY RECAP *********************************************************************
example.wethinkcode.co.za    : ok=2    changed=1    unreachable=0    failed=0
```

# Chapter 5 - DeployStudioAdmin
### 5.1 Introduction

Saving time is a daily concern for system administrators, especially with the increase of minor interventions requested by end-users: accidentally modified system preferences, unstable or misconfigured third-party applications, wrong access rights, viruses, etc...

macOS is stable and robust but the capacity to (re)install completely a workstation or a server in a few minutes following a preset workflow that automatically restores the right disk image, configures and backs up what was planned by the administrator is a real benefit in terms of time and money. DeployStudio was designed this way, in order to manage flawlessly large deployments of workstations and servers.

You will require either a Mac Mini or iMac. The storage will be hosted on the nearest NAS / ZFS.

`OS : Debian 9`

`RAM : 8192MB`

`Storage : 1TB`

### 5.2 Installation

http://www.techrepublic.com/article/how-to-install-and-configure-deploystudio/




--------------






Sources:
OpenLDAP & Kerberos:
https://help.ubuntu.com/lts/serverguide/kerberos-ldap.html
https://help.ubuntu.com/lts/serverguide/openldap-server.html

phpldapadmin:
https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-openldap-and-phpldapadmin-on-an-ubuntu-14-04-server

CRC32 Checksum:
https://niols.fr/blog/ldap-crc

Ansible wikipedia
https://en.wikipedia.org/wiki/Ansible_(software)
